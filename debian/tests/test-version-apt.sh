#!/bin/sh

releases="oldstable stable testing unstable experimental xenial bionic focal jammy"

main(){
echo '
--------------------------------------------------------------------------
'
    for release in $releases; do
	apt-venv $release -c "apt-cache madison apt | grep Source | tail -1"
    done
}

main || exit 77
